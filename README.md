# media-length
Uses mplayer to gather information on video file length for all files in a given directory.

Note that mplayer is required for this script to work.
