#!/bin/bash
#
# A bash script that uses mplayer to count the amount of media
# in a specified directory in hours, days, and weeks.
#
# Holden Lewis
#

TARGET="$PWD/$1"
TOTAL_SECONDS=0
FILE_COUNTER=1
FILES="$(find "$TARGET" -type f)"
TOTAL_FILES="$(echo "$FILES" | wc -l)"

# Separator character used by the for loop.
# Default separates on whitespace.
IFS=$'\n' 
for FILE in $FILES; do
    # -vo null - suppresses video output
    # -ao null - suppresses audio output but keeps video
    # -identify - mplayer will output information about the file
    # -frames 0 will suppress video output
    # First sed expression deletes all lines that don't start with ID_LENGTH=
    # Second sed expression removes the ID_LENGTH= from the start of the line.
    FILE_LENGTH_SEC="$(mplayer -vo null -ao null -identify -frames 0 "$FILE" \
            2> /dev/null | sed -e '/^ID_LENGTH/ !d' -e 's/^ID_LENGTH=//')"
    
    # If FILE_LENGTH_SEC is set.
    if [[ -n "$FILE_LENGTH_SEC" ]]; then
        # Will add the length to the TOTAL_SECONDS.
        # -l option used in bc add decimal point numbers.
        TOTAL_SECONDS=$( echo "$TOTAL_SECONDS+$FILE_LENGTH_SEC" | bc -l);
    fi
    
    # Will update the terminal line instead of making a new line.
    echo -ne "\r$FILE_COUNTER files done out of $TOTAL_FILES"
    ((FILE_COUNTER++))
done
echo # For a new line

TOTAL_HOURS="$(echo "$TOTAL_SECONDS/3600.0" | bc -l)"
TOTAL_DAYS="$(echo $TOTAL_HOURS/24.0 | bc -l)"
TOTAL_WEEKS="$(echo $TOTAL_DAYS/7.0 | bc -l)"
printf "Total hours: %0.2f\n" $TOTAL_HOURS
printf "Total days: %0.2f\n" $TOTAL_DAYS
printf "Total weeks: %0.2f\n" $TOTAL_WEEKS
